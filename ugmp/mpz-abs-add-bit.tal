(
static void
mpz_abs_add_bit (mpz_t d, mp_bitcnt_t bit_index)
{
  mp_size_t dn, limb_index;
  mp_limb_t bit;
  mp_ptr dp;

  dn = GMP_ABS (d->_mp_size);

  limb_index = bit_index / GMP_LIMB_BITS;
  bit = (mp_limb_t) 1 << (bit_index % GMP_LIMB_BITS);

  if (limb_index >= dn)
    {
      mp_size_t i;
      /* The bit should be set outside of the end of the number.
	 We have to increase the size of the number. */
      dp = MPZ_REALLOC (d, limb_index + 1);

      dp[limb_index] = bit;
      for (i = dn; i < limb_index; i++)
	dp[i] = 0;
      dn = limb_index + 1;
    }
  else
    {
      mp_limb_t cy;

      dp = d->_mp_d;

      cy = mpn_add_1 (dp + limb_index, dp + limb_index, dn - limb_index, bit);
      if (cy > 0)
	{
	  dp = MPZ_REALLOC (d, dn + 1);
	  dp[dn++] = cy;
	}
    }

  d->_mp_size = (d->_mp_size < 0) ? - dn : dn;
}
)

@mpz-abs-add-bit ( d* bit-index* -- ) ( -- Z )
  #04 #00 STACK-CHECK-ENTRY
  ;&bit-index STA2
  ;&d STA2

  ;&d LDA2 INC2 INC2 LDA2 ABS2 ;&dn STA2
  ;&bit-index LDA2 #03 SFT2 ;&limb-index STA2
  #01 ;&bit-index LDA2 NIP #07 AND #40 SFT SFT ;&bit STA

  ;&limb-index LDA2 ;&dn LDA2 LTH2 ;&else JCN2
    ;&limb-index LDA2 INC2 ;mpz-alloc JSR2 ;&dp STA2
    ;&dp LDA2 ;&d LDA2 #0004 ADD2 ;&dn LDA2 ;mpn-copyi JSR2
    ;&bit LDA ;&dp LDA2 ;&limb-index LDA2 ADD2 STA
    ;&limb-index LDA2 ;&dn LDA2
    &loop
      EQU2k ,&end JCN
      DUP2 ;&dp LDA2 ADD2 #00 ROT ROT STA
      INC2
      ,&loop JMP
    &end
    POP2 POP2
    ;&limb-index LDA2 INC2 ;&dn STA2
    ;&endif JMP2
  &else
    ;&dn LDA2 INC2 ;mpz-alloc JSR2 ;&dp STA2
    ;&dp LDA2 ;&d LDA2 #0004 ADD2 ;&dn LDA2 ;mpn-copyi JSR2
    ;&dp LDA2 ;&limb-index LDA2 ADD2 DUP2
    ;&dn LDA2 ;&limb-index LDA2 SUB2 ;&bit LDA
    ;mpn-add-1 JSR2
    ;&cy STA
    ;&cy LDA #00 EQU ,&skip JCN
      ;&cy LDA ;&dp LDA2 ;&dn LDA2 ADD2 STA
      ;&dn LDA2 INC2 ;&dn STA2
    &skip
  &endif
  ;&dn LDA2 ;&d LDA2 INC2 INC2 LDA2 SGN2 #ff NEQ ,&nonneg JCN
    NEG2
  &nonneg

  #0000 ;mpz-ptr JSR2 INC2 INC2 STA2
  STACK-CHECK
  JMP2r
  &bit-index $2
  &d $2
  &dn $2
  &limb-index $2
  &dp $2
  &bit $1
  &cy $1
  &function "mpz-abs-add-bit 00
