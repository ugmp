(
static void
mpn_div_qr_pi1 (mp_ptr qp,
		mp_ptr np, mp_size_t nn, mp_limb_t n1,
		mp_srcptr dp, mp_size_t dn,
		mp_limb_t dinv)
{
  mp_size_t i;

  mp_limb_t d1, d0;
  mp_limb_t cy, cy1;
  mp_limb_t q;

  assert (dn > 2);
  assert (nn >= dn);

  d1 = dp[dn - 1];
  d0 = dp[dn - 2];

  assert ((d1 & GMP_LIMB_HIGHBIT) != 0);
  /* Iteration variable is the index of the q limb.
   *
   * We divide <n1, np[dn-1+i], np[dn-2+i], np[dn-3+i],..., np[i]>
   * by            <d1,          d0,        dp[dn-3],  ..., dp[0] >
   */

  i = nn - dn;
  do
    {
      mp_limb_t n0 = np[dn-1+i];

      if (n1 == d1 && n0 == d0)
	{
	  q = GMP_LIMB_MAX;
	  mpn_submul_1 (np+i, dp, dn, q);
	  n1 = np[dn-1+i];	/* update n1, last loop's value will now be invalid */
	}
      else
	{
	  gmp_udiv_qr_3by2 (q, n1, n0, n1, n0, np[dn-2+i], d1, d0, dinv);

	  cy = mpn_submul_1 (np + i, dp, dn-2, q);

	  cy1 = n0 < cy;
	  n0 = n0 - cy;
	  cy = n1 < cy1;
	  n1 = n1 - cy1;
	  np[dn-2+i] = n0;

	  if (cy != 0)
	    {
	      n1 += d1 + mpn_add_n (np + i, np + i, dp, dn - 1);
	      q--;
	    }
	}

      if (qp)
	qp[i] = q;
    }
  while (--i >= 0);

  np[dn - 1] = n1;
}
)

@mpn-div-qr-pi1 ( qp* np* nn* n1 dp* dn* dinv -- )
;&function ;print-str JSR2 #0a .Console/write DEO
  #0c #00 STACK-CHECK-ENTRY
  ;&dinv STA
  ;&dn STA2
  ;&dp STA2
  ;&n1 STA
  ;&nn STA2
  ;&np STA2
  ;&qp STA2
  
  ;&dn LDA2 #0002 GTH2 ;&message1 ROT ASSERT-MESSAGE
  ;&nn LDA2 ;&dn LDA2 LTH2 #00 EQU ;&message2 ROT ASSERT-MESSAGE

  ;&dp LDA2 ;&dn LDA2 DEC2 ADD2 LDA ;&d1 STA
  ;&dp LDA2 ;&dn LDA2 #0002 SUB2 ADD2 LDA ;&d0 STA
  
  ;&d1 LDA #80 AND #00 NEQ ;&message3 ROT ASSERT-MESSAGE

  ;&nn LDA2 ;&dn LDA2 SUB2 ;&i STA2
  &loop
    ;&np LDA2 ;&dn LDA2 DEC2 ;&i LDA2 ADD2 ADD2 LDA ;&n0 STA
    ;&n1 LDA ;&d1 LDA EQU
    ;&n0 LDA ;&d0 LDA EQU
    AND ,&eq JCN
    ,&else JMP

    &eq
      #ff ;&q STA
      ;&np LDA2 ;&i LDA2 ADD2
      ;&dp LDA2
      ;&dn LDA2
      ;&q LDA
      ;mpn-submul-1 JSR2
      POP
      ;&np LDA2 ;&dn LDA2 DEC2 ;&i LDA2 ADD2 ADD2 LDA ;&n1 STA
      ;&endif JMP2

    &else
      ;&n1 LDA
      ;&n0 LDA
      ;&np LDA2 ;&dn LDA2 #0002 SUB2 ;&i LDA2 ADD2 ADD2 LDA
      ;&d1 LDA
      ;&d0 LDA
      ;&dinv LDA
      ;gmp-udiv-qr-3by2 JSR2
      ;&q STA
      ;&n1 STA
      ;&n0 STA

      ;&np LDA2 ;&i LDA2 ADD2
      ;&dp LDA2
      ;&dn LDA2 #0002 SUB2
      ;&q LDA
      ;mpn-submul-1 JSR2
      ;&cy STA

      ;&n0 LDA ;&cy LDA LTH ;&cy1 STA
      ;&n0 LDA ;&cy LDA SUB ;&n0 STA
      ;&n1 LDA ;&cy1 LDA LTH ;&cy STA
      ;&n1 LDA ;&cy1 LDA SUB ;&n1 STA
      ;&n0 LDA ;&np LDA2 ;&dn LDA2 #0002 SUB2 ;&i LDA2 ADD2 ADD2 STA

      ;&cy LDA #00 EQU ,&zero JCN
        ;&np LDA2 ;&i LDA2 ADD2
        ;&np LDA2 ;&i LDA2 ADD2
        ;&dp LDA2
        ;&dn LDA2 DEC2
        ;mpn-add-n JSR2
        ;&d1 LDA
        ADD ;&n1 LDA ADD ;&n1 STA
        ;&q LDA DEC ;&q STA
      &zero

    &endif
    ;&qp LDA2 NULLPTR EQU2 ,&null JCN
      ;&q LDA ;&qp LDA2 ;&i LDA2 ADD2 STA
    &null

    ;&i LDA2 DEC2 ;&i STA2
    ;&i LDA2 SGN2 #ff NEQ ;&loop JCN2

  ;&n1 LDA ;&np LDA2 ;&dn LDA2 DEC2 ADD2 STA
  STACK-CHECK
  JMP2r
  &dinv $1 &dn $2 &dp $2 &n1 $1 &nn $2 &np $2 &qp $2
  &i $2 &d1 $1 &d0 $1 &cy $1 &cy1 $1 &q $1 &n0 $1
  &function "mpn-div-qr-pi1 00
  &message1 "mpn-div-qr-pi1 20 "1 00
  &message2 "mpn-div-qr-pi2 20 "1 00
  &message3 "mpn-div-qr-pi3 20 "1 00
