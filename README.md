# ugmp

Port of mini-gmp to uxn virtual machine.

## Purpose

Arbitrary precision numbers.

So far only integers with addition, subtraction, multiplication.

Division, rational numbers, floats are still todo.

## Usage

See `main.tal` for a simple example.

Run `make` followed by `uxncli main.rom`.

## Differences from mini-gmp

Limb size is 1 byte.

The `mpn` layer (low level) is pretty much the same as mini-gmp.

There is a managed stack for storing higher level data (`mpz_t`) because
`malloc` is not available.  You can get pointers to the stack using
`mpz-ptr`.  Deleting anywhere in the stack is possible, but this
invalidates pointers to higher up the stack than the deletion point.

In-place reallocation is not supported, thus instead of (e.g.)

    mpz_add(r, a, b)

the add function always allocates a new mpz on the top of the stack:

    aptr bptr ;mpz-add JSR2

Source code has been split into many tiny `.tal` files, this is so that
smaller ROMs can be built if you don't need all of them.  Dependency
chasing in that case is up to you, though the regularity of filenames vs
defined symbols should make it easier.

## Legal

ugmp is derived from mini-gmp under combined LGPL/GPL license, so the
same conditions apply to this repository.

The source code files starting with some C code in a comment are usually
more-or-less straight re-implementation of the mini-gmp functions.

Other files have new code, so far this is mainly in `ugmp/mpz.tal`; this
new code is Copyright (C) 2022 Claude Heiland-Allen.

---
https://mathr.co.uk
