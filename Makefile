SOURCES := $(shell ls -1 ugmp/*)

main.rom: main.tal ugmp.tal debug.tal debug-macros.tal ndebug-macros.tal $(SOURCES)
	uxnasm main.tal main.rom

ugmp.tal: $(SOURCES)
	ls -1 ugmp/* | sed "s/^/~/" > ugmp.tal
